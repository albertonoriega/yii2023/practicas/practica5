<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSuma()
    {
        $model = new \app\models\Operaciones();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('sumayresta', [
                    'model' => $model,
                    'c' => 0,
                ]);
            }
        }

        return $this->render('suma', [
            'model' => $model,
            'labelBoton' => 'Sumar',
        ]);
    }

    public function actionResta()
    {
        $model = new \app\models\Operaciones();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('sumayresta', [
                    'model' => $model,
                    'c' => 1,
                ]);
            }
        }

        return $this->render('suma', [
            'model' => $model,
            'labelBoton' => 'Restar',
        ]);
    }

    public function actionProducto()
    {
        $model = new \app\models\Operaciones();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('multiplicarydividir', [
                    'model' => $model,
                    'c' => 0,
                ]);
            }
        }

        return $this->render('multiplicar', [
            'model' => $model,
            'labelBoton' => 'Multiplicar'
        ]);
    }

    public function actionDivision()
    {
        $model = new \app\models\Operaciones();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('multiplicarydividir', [
                    'model' => $model,
                    'c' => 1,
                ]);
            }
        }

        return $this->render('multiplicar', [
            'model' => $model,
            'labelBoton' => 'Dividir'
        ]);
    }
    public function actionOperaciones()
    {
        $model = new \app\models\Operaciones();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('operaciones', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('multiplicar', [
            'model' => $model,
            'labelBoton' => 'Calcular'
        ]);
    }
}
