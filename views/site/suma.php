<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Operaciones $model */
/** @var ActiveForm $form */

?>
<div class="site-suma">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero1')->input('number') ?>
    <?= $form->field($model, 'numero2')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton($labelBoton, ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-suma -->