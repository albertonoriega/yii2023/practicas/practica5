<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'numero1',
        'numero2',
        [
            'label' => 'Suma',
            'value' => $model->sumar()
        ],
        [
            'label' => 'Resta',
            'value' => $model->restar()
        ],
        [
            'label' => 'Producto',
            'value' => $model->producto()
        ],
        [
            'label' => 'División',
            'value' => $model->division()
        ],
    ],
]);
