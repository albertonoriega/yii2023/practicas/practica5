<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'numero1',
        'numero2',
    ]
]);


if ($c == 0) {
?>
    <div>
        El resultado de la suma es: <?= $model->sumar() ?>
    </div>
<?php
} else {
?>
    <div>
        El resultado de la resta es: <?= $model->restar() ?>
    </div>
<?php
}
