<?php

use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Operaciones $model */
/** @var ActiveForm $form */
?>
<div class="site-multiplicar">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numero1', ['inputOptions' => ['value' => $model->numero1]]) ?>
    <?= $form->field($model, 'numero2', ['inputOptions' => ['value' => $model->numero2]]) ?>
    <?php
    if ($c == 0) {
        echo $form->field($model, 'Producto', ['inputOptions' => ['value' => $model->producto()]]);
    } else {
        echo $form->field($model, 'División', ['inputOptions' => ['value' => $model->division()]]);
    }
    ?>


    <?php ActiveForm::end(); ?>

</div><!-- site-multiplicar -->