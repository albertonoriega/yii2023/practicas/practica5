<?php

namespace app\models;

use Yii;
use yii\base\Model;


class Operaciones extends Model
{
    public ?int $numero1 = null;
    public ?int $numero2 = null;
    public ?int $resultado = null;

    public function rules()
    {
        return [
            [['numero1', 'numero2',], 'required'],
            [['numero1', 'numero2', 'resultado'], 'number']
        ];
    }

    public function attributeLabels()
    {
        return [
            'numero1' => 'Número 1',
            'numero2' => 'Número 2',
        ];
    }

    public function sumar()
    {
        return $this->numero1 + $this->numero2;
    }

    public function restar()
    {
        return $this->numero1 - $this->numero2;
    }
    public function producto()
    {
        return $this->numero1 * $this->numero2;
    }

    public function division()
    {
        return $this->numero1 /     $this->numero2;
    }
}
